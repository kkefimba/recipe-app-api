from django.test import TestCase
from django.contrib.auth import get_user_model


class ModelTests(TestCase):
    def test_create_user_with_email_successful(self):
        """test creating a new user with a email successful"""
        email = "test@test.com"
        password = "1234jshbfdjlhfb"
        user = get_user_model().objects.create_user(email=email, password=password)
        self.assertEqual(user.email, email)
        self.assertTrue(user.check_password(password))

    def test_new_user_normalize(self):
        """test the email for a new user is normalized"""
        email = "test@DOMAIN.COM"
        user = get_user_model().objects.create_user(email, "enamil123")
        self.assertEqual(user.email, email.lower())

    def test_new_user_invalid_email(self):
        """test creating email with no email raise error"""
        with self.assertRaises(ValueError):
            get_user_model().objects.create_user(None, "testpwd")

    def test_create_new_superuser(self):
        """Test creating super user"""
        user = get_user_model().objects.create_superuser(
            "test@superuser.com", "pwdadmin"
        )

        self.assertTrue(user.is_superuser)
        self.assertTrue(user.is_staff)
